package agency.sevenofnine.testwebview

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.*
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*



class WVClient(val activity: AppCompatActivity, val username: String, val password: String, val requests: MutableList<String>,
               val adapter: ArrayAdapter<String>): WebViewClient() {
    override fun onReceivedHttpAuthRequest(view: WebView?, handler: HttpAuthHandler?, host: String?, realm: String?) {
        handler?.proceed(username, password)
    }



    override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
        val logMsg = "${request?.method}, ${request?.url}"
        Log.d("xxx", logMsg)
        activity.runOnUiThread{
            requests.add(logMsg)
            adapter.notifyDataSetChanged();
        }
        return super.shouldInterceptRequest(view, request)
    }
}

class MainActivity : AppCompatActivity() {

    private val requests = mutableListOf<String>()
    private lateinit var adapter: ArrayAdapter<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, requests)
        request_list.adapter = adapter
        url.setText("https://myvalamartest.broj42.com/");

        webview.settings.javaScriptEnabled = true
        webview.settings.javaScriptCanOpenWindowsAutomatically = true
        load.setOnClickListener {
            webview.webViewClient = WVClient(this, username.text.toString(), password.text.toString(), requests, adapter)
            val headers = mapOf<String, String>("wvfn" to "test", "wvln" to "sanja", "wvrid" to "9902152",
                                                "wva" to "PHCRY",  "wvr" to "107", "wvsd" to "09-12-2017",
                                                "wved" to "31-12-2018")
            webview.loadUrl(url.text.toString(), headers)
            hideKeyboard();
            requests.clear();
            adapter.notifyDataSetChanged()
        }


    }

    fun hideKeyboard() {
        val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = this.currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (webview.canGoBack()) {
                        webview.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }

        }
        return super.onKeyDown(keyCode, event)
    }
}


